# CRUD

#### 介绍
通过 SSM 框架完成简单的 CRUD 功能

#### 软件架构
CRUD 项目基于 Spring、SpringMVC、MyBatis、PageHelper、Taglibs、Jackson、Bootstrap 等技术开发


#### 安装教程

[本地部署步骤详细说明文档](https://www.cnblogs.com/xiqingbo/p/java-30.html)

#### 使用说明

1. 替换上面部署文档中第 1 步的远程仓库地址为：[https://gitee.com/xiqingbo/crud.git](https://gitee.com/xiqingbo/crud.git)

2. 在数据库管理工具中新建数据库名固定为 `spring`，并替换第 13 步初始化步骤为以下文件中的表及数据：[spring.sql](https://files-cdn.cnblogs.com/files/xiqingbo/CRUD%E9%A1%B9%E7%9B%AE%E5%88%9D%E5%A7%8B%E5%8C%96SQL.rar)

3. 其余部署步骤按文档操作就可以看到以下主页啦！ :cherry_blossom: 

![输入图片说明](target/CRUD%E9%A1%B9%E7%9B%AE%E4%B8%BB%E9%A1%B5.png)



