package pers.schieber.crud.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pers.schieber.crud.bean.Department;
import pers.schieber.crud.bean.Message;
import pers.schieber.crud.service.DepartmentService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @PackageName: pers.schieber.crud.controller
 * @ClassName: DepartmentController
 * @Description: 部门页面表现层
 * @Author: Schieber
 * @Date: 2021/4/12 上午 2:16
 */
@Controller
public class DepartmentController {

    @Resource
    private DepartmentService departmentService;

    @ResponseBody
    @RequestMapping(value = "/departments")
    public Message findAllDepartments() {
        List<Department> departmentList = departmentService.findAllDepartments();

        return Message.success().add("departmentList", departmentList);
    }
}
