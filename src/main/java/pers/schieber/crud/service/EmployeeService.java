package pers.schieber.crud.service;

import org.springframework.stereotype.Service;
import pers.schieber.crud.bean.Employee;
import pers.schieber.crud.bean.EmployeeExample;
import pers.schieber.crud.dao.EmployeeMapper;

import javax.annotation.Resource;
import java.util.List;

/**
 * @PackageName: pers.schieber.crud.service
 * @ClassName: EmployeeService
 * @Description: 员工业务逻辑层
 * @Author: Schieber
 * @Date: 2021/4/10 下午 19:37
 */
@Service
public class EmployeeService {

    @Resource
    private EmployeeMapper employeeMapper;

    /**
     * 添加员工
     *
     * @param employee 添加的员工对象
     * @return 返回添加操作后数据库返回的行数
     */
    public int addEmployee(Employee employee) {
        return employeeMapper.insertSelective(employee);
    }


    /**
     * 修改员工
     *
     * @param employee 修改员工的对象
     * @return 返回修改员工后的影响行数
     */
    public int updateEmployee(Employee employee) {
        return employeeMapper.updateByPrimaryKeySelective(employee);
    }

    /**
     * 查找所有员工
     *
     * @return 返回查找出的员工列表
     */
    public List<Employee> findAllEmployees() {
        EmployeeExample employeeExample = new EmployeeExample();
        employeeExample.setOrderByClause("e.emp_id asc");
        return employeeMapper.selectByExampleWithDept(employeeExample);
    }


    /**
     * 校验员工姓名是否重复与可用
     *
     * @param empName 传入员工姓名
     * @return 返回0说明数据库中没有该员工姓名为true代表可用，反之亦然
     */
    public boolean checkEmpName(String empName) {
        EmployeeExample employeeExample = new EmployeeExample();
        EmployeeExample.Criteria criteria = employeeExample.createCriteria();
        criteria.andEmpNameEqualTo(empName);
        long count = employeeMapper.countByExample(employeeExample);
        return count == 0;
    }

    /**
     * 通过主键id查询员工信息
     *
     * @param id 员工id
     * @return 返回根据主键id查询出的员工对象
     */
    public Employee findEmployeeById(Integer id) {
        return employeeMapper.selectByPrimaryKey(id);

    }

    /**
     * 通过id删除员工
     *
     * @param empId 删除的员工Id
     * @return 返回删除操作后数据库的影响行数
     */
    public int deleteEmployee(Integer empId) {
        return employeeMapper.deleteByPrimaryKey(empId);
    }

    /**
     * 批量删除员工
     *
     * @param employeesId 批量删除的员工id集合
     * @return 返回删除操作后影响数据库的影响行数
     */
    public int batchDeleteEmployees(List<Integer> employeesId) {
        EmployeeExample employeeExample = new EmployeeExample();
        employeeExample.createCriteria().andEmpIdIn(employeesId);
        return employeeMapper.deleteByExample(employeeExample);
    }
}
