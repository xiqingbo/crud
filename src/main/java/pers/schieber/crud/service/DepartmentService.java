package pers.schieber.crud.service;

import org.springframework.stereotype.Service;
import pers.schieber.crud.bean.Department;
import pers.schieber.crud.dao.DepartmentMapper;

import javax.annotation.Resource;
import java.util.List;

/**
 * @PackageName: pers.schieber.crud.service
 * @ClassName: DepartmentService
 * @Description: 部门业务逻辑层
 * @Author: Schieber
 * @Date: 2021/4/12 上午 2:17
 */
@Service
public class DepartmentService {

    @Resource
    private DepartmentMapper departmentMapper;


    /**
     * 查找所有部门信息
     *
     * @return 返回查找后查询的部门列表
     */
    public List<Department> findAllDepartments() {
        return departmentMapper.selectByExample(null);
    }
}
