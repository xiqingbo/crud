package pers.schieber.crud.bean;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

/**
 * @author Administrator
 */
public class Employee {
    private Integer empId;

    @Pattern(regexp = "(^[a-zA-Z][a-zA-Z0-9_]{4,14}$)|(^[\\u4e00-\\u9fa5_a-zA-Z0-9_]{2,5})$", message = "员工姓名应在【英文5-15位，中文2-5位】区间内！")
    private String empName;

    private String gender;

    @Email
    private String mailbox;

    private Integer deptId;

    private Department department;

    public Employee(Integer empId, String empName, String gender, String mailbox, Integer deptId) {
        this.empId = empId;
        this.empName = empName;
        this.gender = gender;
        this.mailbox = mailbox;
        this.deptId = deptId;
    }

    public Employee() {
    }

    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName == null ? null : empName.trim();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender == null ? null : gender.trim();
    }

    public String getMailbox() {
        return mailbox;
    }

    public void setMailbox(String mailbox) {
        this.mailbox = mailbox == null ? null : mailbox.trim();
    }

    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}