package pers.schieber.crud.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pers.schieber.crud.bean.Department;
import pers.schieber.crud.bean.Employee;
import pers.schieber.crud.dao.DepartmentMapper;
import pers.schieber.crud.dao.EmployeeMapper;

import java.util.UUID;

/**
 * @PackageName: pers.schieber.crud.test
 * @ClassName: MapperTest
 * @Description: 测试映射SQL运行
 * @Author: Schieber
 * @Date: 2021/4/9 下午 13:31
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class MapperTest {

    @Autowired
    private DepartmentMapper departmentMapper;

    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 测试DepartmentMapper
     */
    @Test
    public void testCrud() {
/*        departmentMapper.insertSelective(new Department(null, "开发部"));
        departmentMapper.insertSelective(new Department(null, "测试部"));*/

        for (int i = 0; i < 30; i++) {
            // 定义通用唯一识别码
            String uuid = UUID.randomUUID().toString().substring(0, 5) + i;
            employeeMapper.insertSelective(new Employee(null, uuid, "M", uuid + "@qq.com", 1));
        }
        System.out.println("批量添加成功");
    }
}
