package pers.schieber.crud.test;

import com.github.pagehelper.PageInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pers.schieber.crud.bean.Employee;

import java.util.List;

/**
 * @PackageName: pers.schieber.crud.test
 * @ClassName: MvcTest
 * @Description: 使用Spring测试模块提供的测试功能，测试CRUD请求的正确性
 * @Author: Schieber
 * @Date: 2021/4/10 下午 22:48
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"classpath:applicationContext.xml", "file:src/main/webapp/WEB-INF/dispatcherServlet-servlet.xml"})
public class MvcTest {

    // 传入SpringMVC的IOC容器
    @Autowired
    WebApplicationContext webApplicationContext;

    // 虚拟MVC请求，获取到处理结果
    MockMvc mockMvc;

    @Before
    public void initMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testPage() throws Exception {
        // 模拟请求拿到返回值
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/employees").param("pageNum", "1")).andReturn();

        // 请求成功后，请求域中会有pageInfo，取出pageInfo进行验证
        MockHttpServletRequest request = mvcResult.getRequest();
        PageInfo pageInfo = (PageInfo) request.getAttribute("pageInfo");
        System.out.println("当前页码：" + pageInfo.getPageNum());
        System.out.println("总页码：" + pageInfo.getPages());
        System.out.println("总记录数：" + pageInfo.getTotal());
        System.out.println("在页面导航需要连续显示的页码：");
        int[] navigatePages = pageInfo.getNavigatepageNums();
        for (int navigatePage : navigatePages
        ) {
            System.out.println(navigatePage);
        }

        // 获取员工数据
        List<Employee> list = pageInfo.getList();
        for (Employee employee : list
        ) {
            System.out.println("ID:" + employee.getEmpId() + " ---> Name:" + employee.getEmpName());
        }
    }
}
